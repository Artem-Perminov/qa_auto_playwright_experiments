import { ComponentProps } from '../../types/pageFactory/baseElement';
import { expect, test } from '@playwright/test';

export class Steps {
  public static async clickOn(element: ComponentProps) {
    await test.step(`Clicking the element with name "${element.name}"`, async () => {
      const locator = element.page.locator(element.locator);
      await locator.click();
    });
  }

  public static async checkThatElementIsVisible(element: ComponentProps) {
    return await test.step(`Element with name "${element.name}" should be visible on the page`, async () => {
      const locator = element.page.locator(element.locator);
      await expect(locator).toBeVisible();
    });
  }
}
