import { BasePage } from './basePage';
import { Logo } from '../pageFactory/logo';
import { Page } from '@playwright/test';
import { Button } from '../pageFactory/button';
import { Title } from '../pageFactory/title';
import { CardForm } from '../components/cardForm';

export class MainPage extends BasePage {
  readonly tinkoffLogo: Logo;
  readonly participateHeaderBtn: Button;
  readonly howGameTitle: Title;
  readonly arenaBannerTitle: Title;
  readonly gotCardBtn: Button;
  readonly cardForm: CardForm;

  constructor(public page: Page) {
    super(page);

    this.cardForm = new CardForm(page);
    this.tinkoffLogo = new Logo({ page, locator: '[class*="Header_link"]', name: 'Tinkoff x КХЛ' });
    this.participateHeaderBtn = new Button({
      page,
      locator: 'a[class*="ButtonLink_root_button_primary"][class*="ArenaBanner_button"]',
      name: 'Participate',
    });
    this.gotCardBtn = new Button({
      page,
      locator: '[class*="button_secondary"]',
      name: 'Got card',
    });
    this.howGameTitle = new Title({ page, locator: '[class*="Rules_title"]', name: 'Rules title' });
    this.arenaBannerTitle = new Title({
      page,
      locator: '[class*="ArenaBanner_title"]',
      name: 'Arena banner title',
    });
  }
}
