import { expect, Page, test } from '@playwright/test';
import { Footer } from '../components/footer';

export class BasePage {
  readonly footer: Footer;

  constructor(protected page: Page) {
    this.footer = new Footer(page);
  }

  async visit(url: string): Promise<void> {
    await test.step(`Opening the url "${url}"`, async () => {
      await this.page.goto(url);
    });
  }

  async reload(): Promise<void> {
    const currentUrl = this.page.url();

    await test.step(`Reloading page with url "${currentUrl}"`, async () => {
      await this.page.reload({ waitUntil: 'domcontentloaded' });
    });
  }

  async titleIsCorrect(title: string): Promise<void> {
    await test.step(`Page title to equal ${title} `, async () => {
      await expect(this.page).toHaveTitle(title);
    });
  }
}
