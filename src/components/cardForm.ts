import { Page } from '@playwright/test';
import { Form } from '../pageFactory/form';
import { MultiSelect } from '../pageFactory/multiSelect';
import { CheckBox } from '../pageFactory/checkBox';
import { Button } from '../pageFactory/button';
import { Input } from '../pageFactory/input';
import { Title } from '../pageFactory/title';

export class CardForm {
  readonly tinkoff: Form;
  readonly cashbackCategories: MultiSelect;
  readonly firstItem: CheckBox;
  readonly secondItem: CheckBox;
  readonly thirdItem: CheckBox;
  readonly fourthItem: CheckBox;
  readonly checkoutButton: Button;
  readonly userName: Input;
  readonly userPhone: Input;
  readonly userEmail: Input;
  readonly userDateBirth: Input;
  readonly numberConfirmation: Title;

  constructor(public page: Page) {
    this.tinkoff = new Form({
      page,
      locator: '[data-qa-type="uikit/screenTwinCols.withSidebar.form"]',
      name: 'Tinkoff Black Card',
      iframe: '[class*="form_active"]',
    });

    this.cashbackCategories = new MultiSelect({
      page,
      locator: '[data-qa-type="uikit/multiSelect"]',
      name: 'Cashback categories',
      iframe: '[class*="form_active"]',
    });

    this.firstItem = new CheckBox({
      page,
      locator: '[data-qa-type="uikit/multiSelect.dropdown.item"][data-dropdown-item-index="0_0"]',
      name: 'First checkbox',
      iframe: '[class*="form_active"]',
    });

    this.secondItem = new CheckBox({
      page,
      locator: '[data-qa-type="uikit/multiSelect.dropdown.item"][data-dropdown-item-index="0_1"]',
      name: 'Second checkbox',
      iframe: '[class*="form_active"]',
    });

    this.thirdItem = new CheckBox({
      page,
      locator: '[data-qa-type="uikit/multiSelect.dropdown.item"][data-dropdown-item-index="0_2"]',
      name: 'Third checkbox',
      iframe: '[class*="form_active"]',
    });

    this.fourthItem = new CheckBox({
      page,
      locator: '[data-qa-type="uikit/multiSelect.dropdown.item"][data-dropdown-item-index="0_3"]',
      name: 'Fourth checkbox',
      iframe: '[class*="form_active"]',
    });

    this.checkoutButton = new Button({
      page,
      locator: '[data-qa-type="uikit/button"]',
      name: 'Checkout checkbox',
      iframe: '[class*="form_active"]',
    });

    this.userName = new Input({
      page,
      locator: '[data-qa-type="uikit/inputFio.value.input"]',
      name: 'User name',
      iframe: '[class*="form_active"]',
    });

    this.userPhone = new Input({
      page,
      locator: '[data-qa-type="uikit/inputPhone.value.input"]',
      name: 'User phone',
      iframe: '[class*="form_active"]',
    });

    this.userEmail = new Input({
      page,
      locator: '[data-qa-type="uikit/inputAutocomplete.value.input"]',
      name: 'User email',
      iframe: '[class*="form_active"]',
    });

    this.userEmail = new Input({
      page,
      locator: '[data-qa-type="uikit/inputAutocomplete.value.input"]',
      name: 'User email',
      iframe: '[class*="form_active"]',
    });

    this.userDateBirth = new Input({
      page,
      locator: '[data-qa-type="uikit/input.value.input"]',
      name: 'User date birth',
      iframe: '[class*="form_active"]',
    });

    this.numberConfirmation = new Title({
      page,
      locator:
        '//*[@data-qa-type="uikit/sectionTitle" and contains(., \'Подтвердите мобильный телефон\')]',
      name: 'Confirmation of the number',
      iframe: '[class*="form_active"]',
    });
  }
}
