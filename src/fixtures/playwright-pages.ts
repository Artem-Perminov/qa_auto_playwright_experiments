import { MainPage } from '../pages/mainPage';
import { Fixtures, PlaywrightTestArgs } from '@playwright/test';

export interface PlaywrightPagesFixture {
  mainPage: MainPage;
}

export const pagesFixture: Fixtures<PlaywrightPagesFixture, PlaywrightTestArgs> = {
  mainPage: async ({ page }, use) => {
    const mainPage = new MainPage(page);

    await use(mainPage);
  },
};
