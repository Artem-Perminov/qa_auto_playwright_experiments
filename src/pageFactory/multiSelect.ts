import { BaseElement } from './baseElement';

export class MultiSelect extends BaseElement {
  readonly typeOf = 'multi select';
}
