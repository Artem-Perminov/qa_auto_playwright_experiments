import { BaseElement } from './baseElement';

export class Title extends BaseElement {
  readonly typeOf: string = 'title';
}
