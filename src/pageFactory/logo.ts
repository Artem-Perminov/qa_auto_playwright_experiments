import { BaseElement } from './baseElement';

export class Logo extends BaseElement {
  readonly typeOf: string = 'logo';
}
