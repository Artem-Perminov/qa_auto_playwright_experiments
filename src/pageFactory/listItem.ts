import { BaseElement } from './baseElement';
import { LocatorProps } from '../../types/pageFactory/baseElement';
import { expect, test } from '@playwright/test';

export class ListItem extends BaseElement {
  readonly typeOf = 'list item';

  async checkSpecificText(text: string, locatorProps: LocatorProps = {}) {
    await test.step(`Checking that ${this.typeOf} with name "${this.componentName}", have ${text}`, async () => {
      const locator = this.getLocator(locatorProps);

      const texts = await locator.allTextContents();
      const arrText = [];

      for (const el of texts) {
        arrText.push(el.trim());
      }

      const result: string[] = texts.filter((el) => el === text);

      expect(result.join('')).toBe(text);
    });
  }
}
