import { expect, Locator, Page, test } from '@playwright/test';
import { ComponentProps, LocatorProps } from '../../types/pageFactory/baseElement';
import { locatorTemplateFormat } from '../utils/pageFactory';
import { capitalizeFirstLetter } from '../utils/generic';

export abstract class BaseElement {
  page: Page;
  locator: string;
  readonly name: string | undefined;
  readonly iframe: string | undefined;

  readonly typeOf: string = 'baseElement';

  constructor({ page, locator, name, iframe }: ComponentProps) {
    this.page = page;
    this.locator = locator;
    this.name = name;
    this.iframe = iframe;
  }

  get typeOfUpper(): string {
    return capitalizeFirstLetter(this.typeOf);
  }
  get componentName(): string {
    if (!this.name) {
      throw Error('Provide "name" property to use "componentName"');
    }

    return this.name;
  }

  getLocator(props: LocatorProps = {}): Locator {
    const { locator, ...context } = props;
    const withTemplate = locatorTemplateFormat(locator || this.locator, context);

    if (!this.iframe) {
      return this.page.locator(withTemplate);
    }
    return this.page.frameLocator(this.iframe).locator(withTemplate);
  }

  async shouldBeVisible(locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`${this.typeOfUpper} with name "${this.componentName}" should be visible on the page`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toBeVisible();
    });
  }

  async shouldNotBeVisible(locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`${this.typeOfUpper} with name "${this.componentName}" should not be visible on the page`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toBeHidden();
    });
  }

  async shouldHaveText(text: string, locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`${this.typeOfUpper} with name "${this.componentName}" should have text "${text}"`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toContainText(text);
    });
  }

  async click(locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`Clicking the ${this.typeOf} with name "${this.componentName}"`, async () => {
      const locator = this.getLocator(locatorProps);
      await locator.click();
    });
  }

  async toBeInViewport(locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`${this.typeOfUpper} with name "${this.componentName}" should be visible in viewport`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toBeInViewport();
    });
  }

  async notToBeInViewport(locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`${this.typeOfUpper} with name "${this.componentName}" should not be visible in viewport`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).not.toBeInViewport();
    });
  }

  async checkCss(key: string, value: string, locatorProps: LocatorProps = {}) {
    await test.step(`Checking CSS for ${this.typeOf} with name "${this.componentName}", ${key} = ${value}`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toHaveCSS(key, value);
    });
  }

  async checkAttribute(key: string, value: string, locatorProps: LocatorProps = {}) {
    await test.step(`Checking attribute for ${this.typeOf} with name "${this.componentName}", ${key} = ${value}`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toHaveAttribute(key, value);
    });
  }
}
