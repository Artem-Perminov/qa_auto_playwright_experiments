import { BaseElement } from './baseElement';

export class Form extends BaseElement {
  readonly typeOf: string = 'form';
}
