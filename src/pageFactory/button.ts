import { BaseElement } from './baseElement';
import { LocatorProps } from '../../types/pageFactory/baseElement';
import { test } from '@playwright/test';

export class Button extends BaseElement {
  readonly typeOf: string = 'button';

  async hover(locatorProps: LocatorProps = {}): Promise<void> {
    await test.step(`Hovering the ${this.typeOf} with name "${this.componentName}"`, async () => {
      const locator = this.getLocator(locatorProps);
      await locator.hover();
    });
  }

  async doubleClick(locatorProps: LocatorProps = {}) {
    await test.step(`Double clicking ${this.typeOf} with name "${this.componentName}"`, async () => {
      const locator = this.getLocator(locatorProps);
      await locator.dblclick();
    });
  }
}
