import { BaseElement } from './baseElement';

export class Link extends BaseElement {
  readonly typeOf = 'link';
}
