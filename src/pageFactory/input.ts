import { BaseElement } from './baseElement';
import { LocatorProps } from '../../types/pageFactory/baseElement';
import { expect, test } from '@playwright/test';

type FillProps = { validateValue?: boolean } & LocatorProps;

export class Input extends BaseElement {
  readonly typeOf = 'input';

  async fill(value: string, fillProps: FillProps = {}) {
    const { ...locatorProps } = fillProps;

    await test.step(`Fill ${this.typeOf} with name "${this.componentName}" to value "${value}"`, async () => {
      const locator = this.getLocator(locatorProps);
      await locator.fill(value);
    });
  }

  async shouldHaveValue(value: string, locatorProps: LocatorProps = {}) {
    await test.step(`Checking that ${this.typeOf} with name "${this.componentName}" has a value "${value}"`, async () => {
      const locator = this.getLocator(locatorProps);
      await expect(locator).toHaveValue(value);
    });
  }
}
