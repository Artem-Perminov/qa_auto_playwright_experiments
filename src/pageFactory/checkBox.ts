import { BaseElement } from './baseElement';

export class CheckBox extends BaseElement {
  readonly typeOf = 'check box';
}
