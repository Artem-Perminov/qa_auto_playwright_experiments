import { Page } from '@playwright/test';

export interface ComponentProps {
  page: Page;
  name?: string;
  locator: string;
  iframe?: string;
}

export type LocatorContext = Record<string, string | boolean | number>;

export type LocatorProps = { locator?: string } & LocatorContext;
