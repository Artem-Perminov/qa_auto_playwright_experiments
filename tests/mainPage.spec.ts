import { testExtension as test } from './tests';
import {
  mainPageTitle,
  userDateBirth,
  userEmail,
  userName,
  userPhone,
} from '../src/fixtures/texts';
import { Steps } from '../src/steps/steps';
import { purpleColor, whiteColor } from '../src/fixtures/css';
import * as process from 'node:process';

test.beforeEach(async ({ mainPage }) => {
  await mainPage.visit('/');
});

test.afterEach(async ({}, testInfo) => {
  console.log(`Finished ${testInfo.title} with status ${testInfo.status}`);
});

test.describe('Main page tests', () => {
  test.use({ viewport: { width: 1620, height: 880 } });

  test('@smoke - Logo is visible', async ({ mainPage }, testInfo) => {
    console.log(process.env);
    console.log(testInfo.title);
    await Steps.clickOn(mainPage.tinkoffLogo);
    await Steps.checkThatElementIsVisible(mainPage.tinkoffLogo);
    await mainPage.tinkoffLogo.shouldBeVisible();
    //
    // await expect(mainPage.page.locator('[class*="Header_link"]')).toHaveScreenshot();
    //
    await mainPage.participateHeaderBtn.shouldBeVisible();
    await mainPage.tinkoffLogo.click();
  });

  test('Page scrolls when click on participate header button', async ({ mainPage }) => {
    await mainPage.howGameTitle.notToBeInViewport();
    await Steps.clickOn(mainPage.participateHeaderBtn);
    await mainPage.howGameTitle.toBeInViewport();
  });

  test('Page has correct title', async ({ mainPage }) => {
    await mainPage.titleIsCorrect(mainPageTitle);
  });

  test('Arena title banner is visible', async ({ mainPage }) => {
    await mainPage.arenaBannerTitle.shouldBeVisible();
  });

  test('Flow of card registration by the user', async ({ mainPage }) => {
    await mainPage.gotCardBtn.checkCss('background-color', whiteColor);
    await mainPage.gotCardBtn.hover();
    await mainPage.gotCardBtn.checkCss('background-color', purpleColor);
    await mainPage.cardForm.tinkoff.notToBeInViewport();
    await mainPage.gotCardBtn.click();
    await mainPage.cardForm.tinkoff.toBeInViewport();
    await mainPage.cardForm.cashbackCategories.checkAttribute('aria-expanded', 'false');
    await mainPage.cardForm.cashbackCategories.click();
    await mainPage.cardForm.cashbackCategories.checkAttribute('aria-expanded', 'true');
    await mainPage.cardForm.firstItem.click();
    await mainPage.cardForm.secondItem.click();
    await mainPage.cardForm.thirdItem.click();
    await mainPage.cardForm.fourthItem.click();
    await mainPage.cardForm.userName.fill(userName);
    await mainPage.cardForm.userPhone.fill(userPhone);
    await mainPage.cardForm.userEmail.fill(userEmail);
    await mainPage.cardForm.userDateBirth.fill(userDateBirth);
    await mainPage.cardForm.numberConfirmation.shouldNotBeVisible();
    await mainPage.cardForm.checkoutButton.click();
    await mainPage.cardForm.numberConfirmation.shouldBeVisible();
  });
});
