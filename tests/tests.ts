import { test as base } from '@playwright/test';
import { pagesFixture, PlaywrightPagesFixture } from '../src/fixtures/playwright-pages';
import { combineFixtures } from '../src/utils/helpers';

export const testExtension = base.extend<PlaywrightPagesFixture>(combineFixtures(pagesFixture));
